FROM nginx

COPY . .

EXPOSE 8080

CMD ["./main"]
